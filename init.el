(add-to-list 'load-path "/Users/ilia/.emacs.d/lisp")
(load "init-base") 
(load "init-package") 
(load "init-key") 

(setq debug-on-error t)

;;color-theme
;(require 'color-theme)
;(color-theme-initialize)
;(color-theme-billw)
;(color-theme-calm-forest)


(require 'virtualenvwrapper)
(venv-initialize-interactive-shells) ;; if you want interactive shell support
(venv-initialize-eshell) ;; if you want eshell support
(setq venv-location "/Users/Tao/git/omnimw")
